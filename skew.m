function b = skew(x, dim)
% From https://en.wikipedia.org/wiki/Skewness#Sample_skewness

    if nargin == 1
        dim = 1;
    end

    b = mean((x - mean(x, dim)) .^ 3, dim) ./ (std(x, [], dim) .^ 3);
end
