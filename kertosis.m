function g2 = kertosis(x, dim)
% From https://en.wikipedia.org/wiki/Kurtosis#Sample_kurtosis

    if nargin == 1
        dim = 1;
    end

    g2 = (mean((x - mean(x, dim)) .^ 4, dim) ./ (var(x, [], dim) .^ 2)) - 3;
end
