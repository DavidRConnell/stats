function yhat = lowess(x, y, f)
%LOWESS Locally Weighted Scatter-plot Smoothing
%   YHAT = LOWESS(X, Y, F) Takes a column vector of the independent variables
%   X and an equal length column vector of the dependent variable Y along with a
%   parameter F between 0 and 1 to determine the relative range of surronding
%   variables to consider.
%
%   Values of F near 1 lead to smoother regressions while Fs near 0 take into
%   account more high frequency components.
%
%   X is expected to be sorted and Y(i) should be a function of X(i).
%   When using the mex C version the algorithm is more efficient if X has constant
%   step size (i.e. if X(i + 1) - X(i) = c for all i).
%
%   Build with MEX:
%       mex -R2018a lowess.c
%
%   Adopted from:
%       Jeff Burkey's algorithm

    warning('Using Matlab version. For better efficiency build C version with:\n    %s', ...
            'mex -R2018a lowess.c')

    n = length(x);

    m = fix(n * f + 0.5);
    window = zeros(n, 1);
    yhat = zeros(n, 1);
    r1 = ones(n, 1);
   
    for j = 1:n
        % This could be done in a matrix, but need to keep memory footprint
        % small, thus the loop.

        d = abs(x - x(j));
        d = sort(d);
        window(j) = d(m);
        yhat(j) = rwlreg(x, y, n, window(j), r1, x(j));
    end

    for it = 1:2
        e = abs(y - yhat);

        n = length(e);
        s = median(e);

        r = e / (6 * s);
        r = 1 - r .^ 2;
        r = max(0.d0, r);
        r = r .^ 2;

        for j = 1:n
            yhat(j) = rwlreg(x, y, n, window(j), r, x(j));
        end
    end
end

function [yy] = rwlreg(x, y, n, d, r, xx)
    dd = d;
    ddmax = abs(x(n) - x(1));
    if dd == 0.0
        error('Regression:lowess','LOWESS window size = 0. Increase f.');
    else
        while dd <= ddmax
            f = (abs(x - xx) / dd);
            f = 1.0 - f .^ 3;
            w = ((max(0.d0, f)) .^ 3) .* r;
            total = sum(w);
            c = sum(w > 0);
            if c > 3
                break % out of while loop
            else
                dd=1.28 * dd;
            end
        end
    end

    w = w / total;

    [a, b] = wlsq(x, y, w);
    yy=a + b * xx;
end

function [a, b] = wlsq(x,y,w)
    sumw = abs(1 - sum(w));
    if sumw > 1e-10
        error('Regression:wlsq','\nThere is an error in the weights.\nWeights do not equal zero (%10.9f).\n',sumw);
    end
    wxx = sum(w .* x .^ 2);
    wx = sum(w .* x);
    wxy = sum(w .* x .* y);
    wy = sum(w .* y);
    b = (wxy - wy * wx) / (wxx - wx ^ 2);
    a = wy - b * wx;
end
